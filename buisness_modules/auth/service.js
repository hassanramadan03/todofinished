const User = require('../../models/user');
const generateHash = require('../../utils/utlis').generateHash;
const validPassword = require('../../utils/utlis').validPassword;
const creatJwt = require('../../utils/utlis').createJWT;


function register(_user) {
    const Ouser = new User({
        fullname: _user.fullname,
        username: _user.username,
        password: generateHash(_user.password)
    });

    return new Promise(async (resolve, reject) => {
        try {
            const result = await User.findOne({ 'username': _user.username });
            if (result) {
                resolve({ success: false, message: 'username Is Already Existed...!' });
            } else {
                const user = await Ouser.save();
                const {fullname,username,_id}=user
                const token=creatJwt(user)
                user.update({token})
                // const verfiedusername = await mailer_service.sendMail(user.local.username, creatJwt(user));
                if (user) resolve({ success: true,message: 'Successfully registered...!' , user:{fullname,username,_id} ,token});
                else resolve({ success: false, message: 'Registration failed...!' });
            }
        } catch (error) {
            console.log(error)
            reject(error);
        }
    })
}
async function signin(_user) {
    return new Promise(async (resolve, reject) => {
        try {
            
            const _isUser = await User.findOne({ username: _user.username } ) ;
            if (!_isUser) resolve({ success: false, message: 'Invalid username...!' })
            else {
                const isMatch = await validPassword(_user.password, _isUser.password);
                const {fullname,username,_id}=_isUser
                const token=creatJwt(_isUser)
                _isUser.update({token})
                if (isMatch) resolve({ success: true,message: 'Successfully, logged in  ...!'  ,user:{fullname,username,_id}, token  });
                else resolve({ success: false, message: 'Invalid Password...!' });
            }
        } catch (error) {
            reject(error)
        }
    })
}
 


module.exports = {
    register,
    signin 
}