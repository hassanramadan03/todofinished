const express = require('express');
const router = express.Router();
const auth_controller = require('./controller');


router.post('/login',auth_controller.signin);
router.post('/register',auth_controller.signup);

 

module.exports = router;