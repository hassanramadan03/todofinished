const Todo = require('../../models/todo')


const getAll = (userid) => {
    return new Promise(async (resolve, reject) => {
        try {
            const  allToDos = await Todo.find({ userid }).lean()
             resolve({
                success: true,
                message:'all todos successfully loaded',
                allToDos
             })
        } catch (error) {
            reject({
                success: false,
                message: error
            })
        }
    })
}
const addToDo = async (userid, text) => {
    try {
        const _todo = new Todo({  userid,  text })
        const todo = await _todo.save();
        if (todo) return {
            success: true,
            message: 'todo is successfully Created ..!'
        }
    } catch (error) {
        return {
            success: false,
            message: 'todo is not Created successfully  ..!'
        };
    }
}
const updateToDo = async (_id,text) => {
    try {
        const updated = await Todo.findOneAndUpdate({  _id }, {  text  }).lean();
        if (updated) return {
            success: true,
            message: 'todo is successfully updated ..!'
        };
    } catch (error) {
        console.log(error);
        
        return {
            success: false,
            message: 'todo is not updated successfully  ..!'
        };;
    }
}
const deleteToDo = async (_id) => {
    try {
        const deleted = await Todo.deleteOne({ _id }).lean();
        console.log('todo is successfully deleted');
        if (deleted) return {
            success: true,
            message: 'todo is successfully deleted ..!'
        };
    } catch (error) {
        return {
            success: false,
            message: 'todo is not deleted successfully  ..!'
        };;
    }
}


module.exports = {
    getAll,
    addToDo,
    updateToDo,
    deleteToDo
}