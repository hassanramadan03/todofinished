const express = require('express');
const router = express.Router();
const {getAll,addToDo,deleteToDo,updateToDo} = require('./controller');
 

router.get('/all/:id', getAll );
router.post('/add', addToDo );
router.delete('/delete/:id',deleteToDo)
router.patch('/update',updateToDo)


module.exports = router;
