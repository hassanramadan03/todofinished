require('dotenv').config()
const service = require('./service')
const decode = require('../../utils/Auth').decode;

const getAll = async (req, res) => {
    try {
        const token = req.header('Authorization')
        const {id}=req.params
        console.log(id);
        
        const allToDos = await service.getAll(id);
        res.status(200).send(allToDos);
    } catch (error) {
        res.send(error)

    }
}
const addToDo = async (req, res) => {
    try {
        const text = req.body.text;
        const token = req.header('Authorization')
        const {_id}=decode(token).data
       
        
        const todid = await service.addToDo(_id, text);
        res.status(200).json(todid);
    } catch (error) {
        res.status(200).send(error)
    }
}
const updateToDo = async (req, res) => {
    try {
        const {  id,  text } = req.body;
        const todid = await service.updateToDo(id, text);
        res.status(200).json(todid);
    } catch (error) {
        res.status(200).send(error)
    }
}
const deleteToDo = async (req, res) => {
    try {
        const id = req.params.id;
        const deleted = await service.deleteToDo(id);
        res.status(200).json(deleted);
    } catch (error) {
        res.status(200).send(error)
    }
}

module.exports = {
    getAll,
    addToDo,
    updateToDo,
    deleteToDo
}