const todos=require('./todos/route')
const auth=require('./auth/route')

module.exports={
    todos,
    auth
}