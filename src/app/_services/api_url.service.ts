import { Injectable } from '@angular/core';
import {  RequestOptions } from "@angular/http";
import { HttpHeaders } from "@angular/common/http";


@Injectable()
export class ApiUrlService {
    private baseUrl = 'https://too-do.herokuapp.com/';
    public socketUrl='https://too-do.herokuapp.com'
    private header: any = {};
    constructor() {
       let token=localStorage.getItem('token')
        this.header = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Authorization": token,
                "Access-Control-Allow-Origin": "*"
            })
        }
    }
    getHeaders() {
        return new RequestOptions(this.header);
    }
    getApiUrl() {
        return {
            getAllTwts: this.baseUrl + `todos/all`,
            add:this.baseUrl+`todos/add`,
            update:this.baseUrl+`todos/update`,
            delete:this.baseUrl+`todos/delete`,
            login:this.baseUrl+`auth/login`,
            register:this.baseUrl+`auth/register`
           }
    }
}