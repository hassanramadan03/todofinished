import { map } from 'rxjs/operators';
import { ApiUrlService } from "./api_url.service";
import 'rxjs/Rx';
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from "./shared.service";

@Injectable()
export class ToDoService {
    header;
    constructor(private SharedService:SharedService,private http: HttpClient, private apiUrlService: ApiUrlService) {
        let token=localStorage.getItem('token')
        this.header= {  "Content-Type": "application/json" ,"Authorization": token }
        
    }
    getAllTwts(id) {
        let header:any=this.apiUrlService.getHeaders();
        return this.http.get(`${this.apiUrlService.getApiUrl().getAllTwts}/${id}`,header ).map((res:any)=>{
            let {allToDos}=res;
            return allToDos.map((to,index)=>{to.index=index;return to});
        }) 
    }
    addText(text){
        return  this.SharedService.requsetHandler(this.apiUrlService.getApiUrl().add,"POST",{text})
    }
    updateText(body){
        console.log(body);
        
        return  this.SharedService.requsetHandler(this.apiUrlService.getApiUrl().update,"PATCH",body)
    }
    deleteText(id){
        return  this.SharedService.requsetHandler(`${this.apiUrlService.getApiUrl().delete}/${id}`,"DELETE",{})
    }
    



     

   
}
