import { SharedService } from './shared.service';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { ApiUrlService } from "./api_url.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { async } from "@angular/core/testing";

@Injectable()
export class AuthenticationService {
  constructor(
    private SharedService:SharedService,
    private _apiUrlService: ApiUrlService,
    private router: Router
  ) {}
 async login(user) {
    return await this.SharedService.requsetHandler(this._apiUrlService.getApiUrl().login,"POST",user)
  }
 async register(user) {
    return await this.SharedService.requsetHandler(this._apiUrlService.getApiUrl().register,"POST",user)
  }
 async logout() {
  localStorage.removeItem("currentUser");
  localStorage.removeItem("token");
   return this.router.navigate(["/login"]);
  }
  IsloggedIn() {
    return !!localStorage.getItem("currentUser");
  }
  doLogout(){
    return new Promise((resolve,reject)=>{
       
      return (!localStorage.getItem("token"))? resolve(true):this.doLogout()
    })
  }
}
