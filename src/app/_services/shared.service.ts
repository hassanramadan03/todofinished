import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SharedService {
    header;
    constructor(private snackBar: MatSnackBar) {
        let token=localStorage.getItem('token')
        this.header= {  "Content-Type": "application/json" ,"Authorization": token }
     }


    requsetHandler(url,method,body){
        return fetch(url, {
            method,  
            headers: this.header,
            body: JSON.stringify(body)  
          }).then(res=>this.responseHandler(res))
    }

    async responseHandler(res:any){
            const data=await res.json()
            const status=data.success?' Success !':'Failed !'
            if(data.success)this.openSnackBar(data.message,status)
            return data;
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 3000,
        });
    }
}