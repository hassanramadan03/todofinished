import { AuthenticationService } from './../_services/authentication.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(private AuthService:AuthenticationService ) { }
  ngOnInit() {
  }
  logout(){
    this.AuthService.logout()
  }
  IsloggedIn(){
    return  this.AuthService.IsloggedIn()
  }
  username(){
    const user:any=JSON.parse(localStorage.getItem("currentUser"))
    return user.fullname ;
    // return JSON.parse(localStorage.getItem("currentUser")).username;
  }
}
