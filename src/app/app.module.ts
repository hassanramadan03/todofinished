import { AuthenticationService } from './_services/authentication.service';
import { AuthGuard } from './_guards/auth.guard';
import { LoginComponent } from './login/login.component';
import { ToDoService } from './_services/Todo.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { ApiUrlService } from './_services/api_url.service';
import { HttpClientModule } from '@angular/common/http';
import {  SharedService } from './_services/shared.service';
import { HttpModule } from '@angular/http';
import { MatComponentsModule } from './angularComponents';
import { ToDos   } from './todos/todos.component';
import {   DialogOverviewExampleDialog } from './todos/dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ToDos,
    DialogOverviewExampleDialog,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    MatComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    // SellerMangementComponent,

  ],
  providers: [
    ApiUrlService,
    ToDoService,
    HttpClientModule,
    SharedService,
    AuthGuard,
    AuthenticationService
  ],
  entryComponents: [DialogOverviewExampleDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
