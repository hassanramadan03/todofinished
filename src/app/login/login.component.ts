import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {  AuthenticationService } from '../_services/authentication.service';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loading = false;
    submitted = false;
    returnUrl: string;
    msgErr = false;
    rmsgErr = false;
    signin=true;
    user: any = { username: '', password: '' };
    ruser:any={username:'',password:'',password2:'',fullname:''}
    reserr=''
    unmatch=false;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    revert(){
        this.ruser={}
        this.user={}

        this.signin=!this.signin;
        
    }
    async login() {
        if(!this.user.username||!this.user.password)return this.msgErr = true;
        this.submitted = true;
        this.loading = true;
        try {
            const { success, user,token }: any = await this.authenticationService.login(this.user)
           
            if (success) {
                this.msgErr = false;
                this.loading = false;
                localStorage.setItem('currentUser', JSON.stringify(user));
                localStorage.setItem('token',token)
                return  this.router.navigate(['/'])

            }
        } catch (error) {
            this.reserr = error;
             this.loading = false

        }
    }
    async register() {
        console.log(this.ruser);
        
        if(!this.ruser.username||!this.ruser.password||!this.ruser.password2||!this.ruser.fullname)return this.rmsgErr = true;
        if(this.ruser.password!==this.ruser.password2)return this.unmatch = true;
        this.submitted = true;
        this.loading = true;
        try {
            const { success, user,token }: any = await this.authenticationService.register(this.ruser)
            if (success) {
                this.msgErr = false;
                this.loading = false;
                localStorage.setItem('currentUser', JSON.stringify(user));
                localStorage.setItem('token',token)
                return  this.router.navigate(['/'])

            }
        } catch (error) {
            this.reserr = error;
             this.loading = false

        }
    }
}
