import { Component,   Inject } from '@angular/core';
import {   MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {DialogData}from './todo.interface'
@Component({
    selector: 'dialog-overview-example-dialog',
    templateUrl: 'dialog.html',
    styleUrls: ['./todos.component.css']
  })
  
export class DialogOverviewExampleDialog {

    constructor(
      public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
    onSubmit(data){
      console.log(data);
    }
  
  }