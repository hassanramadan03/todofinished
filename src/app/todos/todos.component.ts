import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ToDoService } from '../_services/Todo.service';
import { MatDialog } from '@angular/material';
import {DialogOverviewExampleDialog}from './dialog.component'
import * as moment from 'moment'
export interface DriverData {
  _id: string;
  created_at: string;
  text: string;
}

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})

export class ToDos implements OnInit {
  displayedColumns: string[] = [ 'No.','text','created_at','action' ];
  spinner: boolean = true;
  number:any=1;
  dataSource: MatTableDataSource<DriverData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  id: string;
  created_at: string;
  text: string;
  phone: string;
  errorMessage: string = '';
  isError: Boolean = false;
  constructor ( private ToDoService: ToDoService,  public dialog: MatDialog) {
    this.id=JSON.parse(localStorage.getItem('currentUser'))._id;
  }
  ngOnInit(){
     
    
    this.getTodos();

  }
   
  getTodos(){
    this.ToDoService.getAllTwts(this.id).subscribe((Todos: any) => {
      this.dataSource = new MatTableDataSource(Todos)
      this.dataSource =this.dataSource ;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
    );
  }
  deleteTodo({_id}){
     this.ToDoService.deleteText(_id).then(()=>this.getTodos())
  }
  openDialog(row): void {
    const {text='',_id=undefined}=row;
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '600px',
      data: { id: _id, text:text  }
    });

    dialogRef.afterClosed().subscribe(async ({text}) => {
      if (!text) return alert(`you didn't type any thing ..!`)
      else {
        try {
          _id?this.ToDoService.updateText({id:_id,text}).then(()=>this.getTodos()):
          this.ToDoService.addText(text).then(()=>this.getTodos())

        } catch ({ message }) {
          console.log(message);
          this.errorMessage = message;
          this.isError = true;
        }
      }
    });
  }
   
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  formatTime(date){
      var unix = Math.round(+new Date(date)/1000);
      return moment.unix(unix ).startOf('day').fromNow();
   }

}


