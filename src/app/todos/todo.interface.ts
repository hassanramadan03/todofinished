export interface DialogData {
    text: string;
    created_at: string;
    index: number;
  }