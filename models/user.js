const mongoose = require('mongoose')
const Schema = mongoose.Schema

const User = new Schema({
    token: String,
    username: String,
    fullname: String,
    password: String,
    created_at: {
        type: Date,
        default:new Date()
    }

}, {
    versionKey: false
})


module.exports = mongoose.model('user', User)