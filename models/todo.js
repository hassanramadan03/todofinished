const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ToDos = new Schema({
    userid: String,
    text: String,
    created_at: {
        type: Date,
        default: new Date()
    }

}, {
    versionKey: false
})


module.exports = mongoose.model('Todo', ToDos)